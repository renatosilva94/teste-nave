### Instruções

#### 1 - Executar o CMD e navegar até a pasta do projeto .

#### 2 - Executar o comando "npm install" para instalar as dependências do projeto .

#### 3 - Executar o comando "npm start" .

#### 4 - Abrir uma nova janela do CMD e executar o comando "npm run jsonserver" .

### Considerações em Relação ao Desenvolvimento

#### Alerts: Implementação do Swet Alert V2 para avisar o usuário sobre o sucesso ou erro de uma determinada ação.
#### Loaders: Utilizado loader na página inicial, biblioteca React Loader Spinner.
#### Organização e Padronização do Código: Código separado em pastas Pages e Components.
#### Feedback: Ações são mostradas através do Sweet Alert V2 com uso de Toast e Modal.
