import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import ListarUsuario from './Pages/ListarUsuario';
import CadastroUsuario from './Pages/CadastroUsuario';
import VisualizarUsuario from './Pages/VisualizarUsuario';
import EditarUsuario from './Pages/EditarUsuario';

import 'bootstrap/dist/css/bootstrap.css';
import { Navbar, Nav, NavItem, NavDropdown, MenuItem } from 'react-bootstrap';

const App = () => {
  return (

      <Router>
        <Switch>
          <Route exact path='/' component={ListarUsuario} />
          <Route path='/CadastroUsuario' component={CadastroUsuario} />
          <Route path='/VisualizarUsuario/:id' component={VisualizarUsuario} /> 
          <Route path='/EditarUsuario/:id' component={EditarUsuario} /> 

        </Switch>
      </Router>
  )
}

export default App;
