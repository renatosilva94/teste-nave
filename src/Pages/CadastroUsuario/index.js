import React, { Component } from "react";
import { Button } from "react-bootstrap";
import axios from "axios";
import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter as Router, Route } from "react-router-dom";
import "./styles.css";
import TextField from "@material-ui/core/TextField";
import Swal from "sweetalert2";

import MenuSuperior from "../../Components/MenuSuperior";

class CadastroUsuario extends Component {
  state = {
    data: [],
    id: 0,
    nome: null,
    email: null,
    vaga: null,
    data_nascimento: null,
    intervalIsSet: false,
    idToDelete: null,
    idToUpdate: null,
    objectToUpdate: null
  };

  /* Método CREATE */

  putDataToDB = (nome, vaga, data_nascimento, email) => {
    let currentIds = this.state.data.map(data => data.id);
    let idToBeAdded = 0;
    let re = /[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,4}/gim;

    if (!nome || !email || !vaga || !data_nascimento) {
      const Toast = Swal.mixin({
        toast: true,
        position: "top-end",
        showConfirmButton: false,
        timer: 3000
      });

      Toast.fire({
        type: "error",
        title: "Preencha Todos Os Campos"
      });
    } else if (email == "" || !re.test(email)) {
      const Toast = Swal.mixin({
        toast: true,
        position: "top-end",
        showConfirmButton: false,
        timer: 3000
      });

      Toast.fire({
        type: "error",
        title: "E-Mail Inválido"
      });
    } else {
      while (currentIds.includes(idToBeAdded)) {
        ++idToBeAdded;
      }

      axios
        .post("http://localhost:3001/users", {
          id: idToBeAdded,
          nome: nome,
          vaga: vaga,
          data_nascimento: data_nascimento,
          email: email
        })
        .then(response => {
          this.props.history.push("/");

          Swal.fire({
            position: "center",
            type: "success",
            title: "Usuário Cadastrado Com Sucesso",
            showConfirmButton: false,
            timer: 1500
          });
        });
    }
  };

  render() {
    return (
      <div>
        <MenuSuperior />

        <div className="fundo">
          <h5 className="titulo">Criar usuário</h5>

          <div className="textFieldDiv">
            <TextField
              id="standard-with-placeholder"
              placeholder="Nome"
              margin="normal"
              onChange={e => this.setState({ nome: e.target.value })}
            />

            <br></br>

            <TextField
              id="standard-with-placeholder"
              placeholder="Vaga"
              margin="normal"
              onChange={e => this.setState({ vaga: e.target.value })}
            />

            <br></br>

            <TextField
              id="standard-with-placeholder"
              placeholder="Data de Nascimento"
              margin="normal"
              type="date"
              onChange={e => this.setState({ data_nascimento: e.target.value })}
            />

            <br></br>

            <TextField
              id="standard-with-placeholder"
              placeholder="Email"
              margin="normal"
              onChange={e => this.setState({ email: e.target.value })}
            />
          </div>

          <br></br>

          <Button
            variant="dark"
            size="sm"
            className="btnEnviar"
            onClick={() =>
              this.putDataToDB(
                this.state.nome,
                this.state.vaga,
                this.state.data_nascimento,
                this.state.email
              )
            }
          >
            ENVIAR
          </Button>
        </div>
      </div>
    );
  }
}

export default CadastroUsuario;
