import React, { Component } from "react";
import {
  Button
} from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import "./styles.css";
import TextField from "@material-ui/core/TextField";
import Swal from "sweetalert2";
import axios from "axios";
import MenuSuperior from "../../Components/MenuSuperior";

class VisualizarUsuario extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      idToUpdate: this.props.match.params.id,
      id: this.props.match.params.id,
      objectToUpdate: null,
      updateNome: this.props.nome
    };
  }

  componentDidMount() {
    axios
      .get("http://localhost:3001/users/" + this.state.idToUpdate)
      .then(value => {
        console.log(value);
        this.setState({
          nome: value.data.nome,
          vaga: value.data.vaga,
          data_nascimento: value.data.data_nascimento,
          email: value.data.email
        });
      });
  }

  render() {
    return (
      <div>
        <MenuSuperior />

        <div className="fundo">
          <div className="textFieldDiv">
            <p className="nomeUsuario">{this.state.nome}</p>

            <br></br>

            <p>Vaga: {this.state.vaga}</p>

            <br></br>

            <p>Data de Nascimento: {new Date(this.state.data_nascimento).toLocaleDateString('pt-BR',{timeZone: 'UTC'})}</p>

            <br></br>

            <p>Email: {this.state.email}</p>
          </div>

          <br></br>

          <Link to={`/`}>
            <Button variant="dark" size="sm" className="btnVoltar">
              VOLTAR
            </Button>
          </Link>
        </div>
      </div>
    );
  }
}

export default VisualizarUsuario;
