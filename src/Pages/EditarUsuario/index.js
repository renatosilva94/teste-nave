import React, { Component } from "react";
import { Button } from "react-bootstrap";
import axios from "axios";
import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter as Router, Route } from "react-router-dom";
import "./styles.css";
import TextField from "@material-ui/core/TextField";
import Swal from "sweetalert2";
import MenuSuperior from "../../Components/MenuSuperior";

class EditarUsuario extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      idToUpdate: this.props.match.params.id,
      id: this.props.match.params.id,
      objectToUpdate: null,
      updateNome: this.props.nome
    };
  }

  componentDidMount() {
    //GET de usuários para o Update
    axios
      .get("http://localhost:3001/users/" + this.state.idToUpdate)
      .then(value => {
        console.log(value);
        this.setState({
          updateNome: value.data.nome,
          updateVaga: value.data.vaga,
          updateDataNascimento: value.data.data_nascimento,
          updateEmail: value.data.email
        });
      });
  }

  //Método UPDATE

  updateDB = (
    idToUpdate,
    updateNome,
    updateVaga,
    updateDataNascimento,
    updateEmail
  ) => {
    let objIdToUpdate = this.state.idToUpdate;
    parseInt(idToUpdate);
    this.state.data.forEach(dat => {
      if (dat.id == idToUpdate) {
        objIdToUpdate = dat._id;
      }
    });

    let re = /[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,4}/gim;

    if (!updateNome || !updateEmail || !updateVaga || !updateDataNascimento) {
      const Toast = Swal.mixin({
        toast: true,
        position: "top-end",
        showConfirmButton: false,
        timer: 3000
      });

      Toast.fire({
        type: "error",
        title: "Preencha Todos Os Campos"
      });
    } else if (updateEmail == "" || !re.test(updateEmail)) {
      const Toast = Swal.mixin({
        toast: true,
        position: "top-end",
        showConfirmButton: false,
        timer: 3000
      });

      Toast.fire({
        type: "error",
        title: "E-Mail Inválido"
      });
    } else {
      axios
        .put("http://localhost:3001/users/" + objIdToUpdate, {
          nome: updateNome,
          vaga: updateVaga,
          data_nascimento: updateDataNascimento,
          email: updateEmail
        })
        .then(response => {
          this.props.history.push("/");
          Swal.fire({
            position: "center",
            type: "info",
            title: "Usuário Atualizado Com Sucesso",
            showConfirmButton: false,
            timer: 1500
          });
        });
    }
  };

  render() {
    return (
      <div>
        <MenuSuperior />

        <div className="fundo">
          <h5 className="titulo">Editar usuário</h5>

          <div className="textFieldDiv">
            <TextField
              id="standard-with-placeholder"
              placeholder="Nome"
              margin="normal"
              onChange={e => this.setState({ updateNome: e.target.value })}
              value={this.state.updateNome}
            />

            <br></br>

            <TextField
              id="standard-with-placeholder"
              placeholder="Vaga"
              margin="normal"
              onChange={e => this.setState({ updateVaga: e.target.value })}
              value={this.state.updateVaga}
            />

            <br></br>

            <TextField
              id="standard-with-placeholder"
              placeholder="Data de Nascimento"
              margin="normal"
              type="date"
              onChange={e => this.setState({ updateDataNascimento: e.target.value })
              }
              value={this.state.updateDataNascimento}
            />

            <br></br>

            <TextField
              id="standard-with-placeholder"
              placeholder="Email"
              margin="normal"
              onChange={e => this.setState({ updateEmail: e.target.value })}
              value={this.state.updateEmail}
            />
          </div>

          <br></br>

          <Button
            variant="dark"
            size="sm"
            className="btnEnviar"
            onClick={() =>
              this.updateDB(
                this.state.idToUpdate,
                this.state.updateNome,
                this.state.updateVaga,
                this.state.updateDataNascimento,
                this.state.updateEmail
              )
            }
          >
            SALVAR EDIÇÕES
          </Button>
        </div>
      </div>
    );
  }
}

export default EditarUsuario;
