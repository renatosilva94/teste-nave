import React, { Component } from "react";
import {
  Table,
  Button
} from "react-bootstrap";
import axios from "axios";
import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";
import "./styles.css";
import Swal from "sweetalert2";
import MenuSuperior from "../../Components/MenuSuperior";
import Loader from "react-loader-spinner";

class ListarUsuario extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      idToDelete: null,
      id: 0,
      intervalIsSet: false,
      visible: true
    };
  }

  componentDidMount() {
    /*Listagem de Usuários*/
    fetch("http://localhost:3001/users")
      .then(response => response.json())
      .then(data => this.setState({ data, visible: false }));
  }

  componentWillUnmount() {
    if (this.state.intervalIsSet) {
      clearInterval(this.state.intervalIsSet);
      this.setState({ intervalIsSet: null, isActive: true });
    }
  }

  //Método DELETE
  deleteFromDb(id) {
    axios
      .delete("http://localhost:3001/users/" + id)
      .then(resp => {
        Swal.fire({
          position: "center",
          type: "warning",
          title: "Usuário Deletado Com Sucesso",
          showConfirmButton: false,
          timer: 1500
        });
        this.componentDidMount();
        console.log(resp.data);
      })
      .catch(error => {
        console.log(error);
      });
  }

  render() {
    const { data } = this.state;

    return (
      <div>
        <MenuSuperior />

        <div className="row">
          <div className="col-sm-4">
            <h5 className="tituloPagina">Usuários</h5>
          </div>

          <div className="col-sm-4"></div>

          <div className="col-sm-4">
            <Link to="/CadastroUsuario">
              <Button variant="dark" size="sm" className="btnCriar">
                CRIAR
              </Button>
            </Link>
          </div>
        </div>

        <br></br>

        <div className="col-sm-12">
          <Table responsive striped hover>
            <thead>
              <tr className="table-light">
                <th>ID</th>
                <th>Nome</th>
                <th>Vaga</th>
                <th>Data de Nascimento</th>
                <th>E-Mail</th>
                <th>Ações</th>
              </tr>
            </thead>

            <tbody>
              {this.state.data.map(dat => {
                return (
                  <tr key="dat" className="table-light">
                    <td>
                      <Link to={`/VisualizarUsuario/${dat.id}`}>{dat.id}</Link>
                    </td>
                    <td>{dat.nome}</td>
                    <td>{dat.vaga}</td>
                    <td>{new Date(dat.data_nascimento).toLocaleDateString('pt-BR',{timeZone: 'UTC'})}</td>
                    <td>{dat.email}</td>
                    <td>
                      <Link to={`/EditarUsuario/${dat.id}`}>
                        <Button variant="dark" size="sm">
                          Editar
                        </Button>
                      </Link>
                      &nbsp;&nbsp;
                      <Button
                        variant="dark"
                        size="sm"
                        onClick={() => this.deleteFromDb(dat.id)}
                      >
                        Deletar
                      </Button>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </Table>
        </div>

        <div className="loader">
          <Loader
            type="ThreeDots"
            color="#000"
            height={200}
            width={200}
            visible={this.state.visible}
          />
        </div>
      </div>
    );
  }
}

export default ListarUsuario;
