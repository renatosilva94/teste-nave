import React from "react";
import { Navbar } from "react-bootstrap";

const MenuSuperior = props => {
  return (
    <Navbar className="menuSuperior" bg="dark">
      <Navbar.Brand href="/">
        <img
          src="/img/logo-white.png"
          width="140"
          height="36"
          className="d-inline-block align-top"
          alt="nave.rs"
        />
      </Navbar.Brand>
    </Navbar>
  );
};

export default MenuSuperior;
